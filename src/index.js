import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, IndexRoute, browserHistory} from 'react-router';
import reduxThunk from 'redux-thunk';

import App from './components/app';
import SignIn from './components/auth/signin';
import SignOut from './components/auth/signout';
import SignUp from './components/auth/signup';
import Feature from './components/feature';
import Welcome from './components/welcome'
import requireAuth from './components/hoc/require_authentication'
import reducers from './reducers';

import { AUTH_USER } from './actions/types'

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);

//Update application state before rendering of any component

const store = createStoreWithMiddleware(reducers);
const token = localStorage.getItem('token');
//if we have token consider user as signed in
if(token) {
	// we need to update application state
	store.dispatch({type: AUTH_USER});
}

//IndexRoute is used for the welcome page. It doesn't need any path since this will render for
//root route only

ReactDOM.render(
  <Provider store={store}>
  <Router history={browserHistory}>
  	<Route path="/" component={App}>
  		<IndexRoute component={Welcome} />
    	<Route path="signin" component={SignIn} />
    	<Route path="signout" component={SignOut} />
    	<Route path="signup" component={SignUp} />
    	<Route path="feature" component={requireAuth(Feature)} />
    </Route>
  </Router>
  </Provider>
  , document.querySelector('.root'));
