import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../actions';


const renderField = ({
		input, 
		label, 
		type, 
		meta:{ touched, error }
	}) => (
		<div className="form-group">
			<label>{label}</label>
			<div>
				<input {...input} type={type} className="form-control" />
				{touched && (error && <span className="error">{error}</span>)}
			</div>
		</div>
	);


class SignUp extends Component {

	
	componentWillMount() {
		this.props.setInitialState();
	}

	handleFormSubmit( {email, password, passwordconfirm}) {
		this.props.signupUser( {email, password});
	}

	renderAlert() {

		if(this.props.errorMessage) {
			return (
				<div className="alert alert-danger">
					<strong>OOPS!</strong> {this.props.errorMessage}
				</div>
			);
		}
	}
	
	render() {

		const { handleSubmit} = this.props;
		return (
				<div className="container">
					<form onSubmit= { handleSubmit(this.handleFormSubmit.bind(this)) }>
							
						<Field name="email" component={renderField} type="email" placeholder="test@gmail.com" label="Email:" />
						
						<Field name="password" component={renderField} type="password" label="Password:" />
										
						<Field name="passwordconfirm" component={renderField} type="password" label="Password Confirm:" />
						
						{this.renderAlert()}
						
						<button action="submit" className="btn btn-primary">Sign Up</button>
						

					</form>
				</div>
			);
	}
}

const validate = (values) => {

	const errors = {};

	if(!values.email) {
		errors.email = 'Please enter an email';
	}

	if(!values.password) {
		errors.password = 'Please enter a password';
	}

	if(!values.passwordconfirm) {
		errors.passwordconfirm = 'Please enter a password confirm';
	}


	if(values.password !== values.passwordconfirm) {
		errors.password = 'Passwords must match';
	}
	return errors;
}



function mapStateToProps(state) {
	return { errorMessage: state.auth.error };
}


SignUp = reduxForm({
	form: 'signup',
	validate
})(SignUp);

SignUp = connect(mapStateToProps, actions)(SignUp);

export default SignUp;

