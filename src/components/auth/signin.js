import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import * as actions from '../../actions';
import { connect } from 'react-redux';

class SignIn extends Component {

	handleFormSubmit({ email, password }) {
		this.props.signinUser({email, password});
	}

	renderAlert() {
		if(this.props.errorMessage) {
			return (
				<div className="alert alert-danger">
					<strong>OOPS!</strong> {this.props.errorMessage}
				</div>
				);
		}
	}

	render() {

		const { handleSubmit } = this.props;
		return (
			<div className="container">
				<form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>

					<fieldset className="form-group">
						<label>Email:</label>
						<div>
							<Field name="email" component="input" type="email" placeholder="test@gmail.com" 
							required="required" className="form-control" />
						</div>
					</fieldset>

					<fieldset className="form-group">
						<label>Password:</label>
						<div>
							<Field name="password" component="input" type="password" 
							required="required" className="form-control" />
						</div>
					</fieldset>
					{this.renderAlert()}
					<button action="submit" className="btn btn-primary">Sign In</button>
				</form>
			</div>
		);
	}

}

function mapStateToProps(state) {
	return { errorMessage: state.auth.error };
}

//Decorate with reduxForm(). It will read the intial values props provided by connect()
SignIn = reduxForm({
	form: 'signin'
})(SignIn);

// You have to connect() to any reducers that you wish to connect to yourself
SignIn = connect(mapStateToProps, actions)(SignIn);

export default SignIn;