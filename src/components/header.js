import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';


class Header extends Component {

	getCommonHeader() {
		return [<li key={1}>
					<Link to="#">About</Link>
				</li>,
				<li key={2}>
					<Link to="#">Contact Us</Link>
				</li>
		];
	}

	renderHeader() {
		if(this.props.authenticated) {
			return (
				<li>
					<Link to="/signout"><i className="fa fa-sign-out" aria-hidden="true"></i> Sign out</Link>
				</li>
				);
		}
		else {
			return [<li key={3}>
						<Link to="/signin"><i className="fa fa-sign-in" aria-hidden="true"></i> Sign in</Link>
					</li>,
					<li key={4}>
						<Link to="/signup"><i className="fa fa-user-plus" aria-hidden="true"></i> Sign up</Link>
					</li>
					];
		}
	}

	render() {
		return (
				
				<nav className="navbar navbar-inverse navbar-fixed-top">
					<div className="container">
						<div className="navbar-header">
							<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						        <span className="sr-only">Toggle navigation</span>
						        <span className="icon-bar"></span>
						        <span className="icon-bar"></span>
						        <span className="icon-bar"></span>
						      </button>
						      <Link to="#" className="navbar-brand"><span className="glyphicon glyphicon-picture" aria-hidden="true"></span> IMGS</Link>
						 </div>
																		
    					<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul className="nav navbar-nav">
								{this.getCommonHeader()}
							</ul>

							<ul className="nav navbar-nav navbar-right">
								{this.renderHeader()}
							</ul>
						</div>
					</div>
				</nav>
			
			);
	}
}

function mapStateToProps(state) {
	return { authenticated: state.auth.authenticated };
}

export default connect(mapStateToProps)(Header);
