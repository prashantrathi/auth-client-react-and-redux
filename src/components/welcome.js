import React, { Component } from 'react';


export default class Welcome extends Component {

	render() {
		return (
			<div className="container">
				<div className="jumbotron">
					<h1><i className="fa fa-camera-retro" aria-hidden="true"></i> The Image Gallery</h1>
					<p> A bunch of beautiful images from unsplash.com</p>
				</div>

				<div className="row">
					<div className="col-lg-4 col-sm-6">
						<div className="thumbnail">
							<img src="https://images.unsplash.com/photo-1435777940218-be0b632d06db?dpr=1&auto=format&fit=crop&w=1296&h=729&q=60&cs=tinysrgb" />
						</div>
					</div>
					<div className="col-lg-4 col-sm-6">
						<div className="thumbnail">
							<img src="https://images.unsplash.com/photo-1495132280856-0de542e5f919?dpr=1&auto=format&fit=crop&w=1296&h=729&q=60&cs=tinysrgb" />
						</div>
					</div>
					<div className="col-lg-4 col-sm-6">
						<div className="thumbnail">
							<img src="https://images.unsplash.com/photo-1495137215671-61b3f15aedb0?dpr=1&auto=format&fit=crop&w=1296&h=729&q=60&cs=tinysrgb" />
						</div>
					</div>
					<div className="col-lg-4 col-sm-6">
						<div className="thumbnail">
							<img src="https://images.unsplash.com/photo-1515410564338-1c4e206af591?dpr=1&auto=format&fit=crop&w=1296&h=729&q=60&cs=tinysrgb" />
						</div>
					</div>
					<div className="col-lg-4 col-sm-6">
						<div className="thumbnail">
							<img src="https://images.unsplash.com/photo-1497298332258-e43b236a8e30?dpr=1&auto=format&fit=crop&w=1296&h=729&q=60&cs=tinysrgb" />
						</div>
					</div>
					<div className="col-lg-4 col-sm-6">
						<div className="thumbnail">
							<img src="https://images.unsplash.com/uploads/1413142095961484763cf/d141726c?dpr=1&auto=format&fit=crop&w=1296&h=729&q=60&cs=tinysrgb" />
						</div>
					</div>
					<div className="col-lg-4 col-sm-6">
						<div className="thumbnail">
							<img src="https://images.unsplash.com/photo-1493585552824-131927c85da2?dpr=1&auto=format&fit=crop&w=1296&h=729&q=60&cs=tinysrgb" />
						</div>
					</div>
					<div className="col-lg-4 col-sm-6">
						<div className="thumbnail">
							<img src="https://images.unsplash.com/photo-1495248449765-7ec3db458549?dpr=1&auto=format&fit=crop&w=1296&h=729&q=60&cs=tinysrgb" />
						</div>
					</div>
					<div className="col-lg-4 col-sm-6">
						<div className="thumbnail">
							<img src="https://images.unsplash.com/photo-1514043370531-a00dbd95c42e?dpr=1&auto=format&fit=crop&w=1296&h=729&q=60&cs=tinysrgb" />
						</div>
					</div>
				</div>
			</div>
			);
	}
}