import {
	AUTH_USER,
	AUTH_ERROR,
	UNAUTH_USER,
	ACCESS_FEATURE
} from './types';

import axios from 'axios';
import { browserHistory } from 'react-router';


const ROOT_URL = "http://localhost:3090";

export function setInitialState() {
	return {
		type: UNAUTH_USER
	}
		
}

export function signinUser({email, password}) {

	// submit server request with email/password
	return (dispatch) => {
		axios.post(`${ROOT_URL}/signin`, {
			email, 
			password 
			}
		)
		.then((response) => {

			// raise state update
			dispatch({ type: AUTH_USER });
			
			// save jwt token
			localStorage.setItem('token', response.data.token);

			// go to restricted route (programmatic navigation)
			browserHistory.push('/feature');
		})
		.catch(() => {
			
			//if error	

			// show error to user
			// raise state update of error
			dispatch(authError("Bad Login Info"));

		});
	}
}

export function signupUser({email, password}) {

	// submit server request signup with email/password
	return (dispatch) => {
		axios.post(`${ROOT_URL}/signup`, {
			email, 
			password 
			}
		)
		.then((response) => {
			console.log(response);
			// raise state update
			dispatch({ type: AUTH_USER });
			
			// save jwt token
			localStorage.setItem('token', response.data.token);

			// go to restricted route (programmatic navigation)
			browserHistory.push('/feature');
		})
		.catch(error => {
			if(error.response) {
				//console.log(error.response.data.error);
				dispatch(authError(error.response.data.error))
			}
		});
	}
}

export function authError(error) {
	return {
		type: AUTH_ERROR,
		payload: error
	}
}


export function signoutUser() {
	return (dispatch) => {

		// Unauthenticate user on signout
		dispatch({type: UNAUTH_USER});

		// clear jwt token
		localStorage.removeItem('token');
	};
}

export function accessFeature() {
	return (dispatch) => {
		axios.get(`${ROOT_URL}`,  { headers : {authorization: localStorage.getItem('token')}
		})
		.then(response => {
			
			dispatch({ 
				type: ACCESS_FEATURE, 
				payload: response.data.content 
			})
		});
		
	}
}