# Auth Client
git clone https://bitbucket.org/prashantrathi/auth-client-react-and-redux

## username
Username for 'https://bitbucket.org': prashant.8579@gmail.com

## password
Password for 'https://prashant.8579@gmail.com@bitbucket.org': (Enter bitbucket account password)

# STEPS TO RUN THE CLIENT
1. After git clone run: cd auth-client-react-and-redux

# To install all required node modules
2. npm install

# Run the client
3. npm run start (this will start the client at port 8080)

### (This will show client static pages. To talk the server and check the siginin, signup or signout routes, start the server using repository "auth-server-express-and-mongodb").

## Using steps given here and at "auth-server-express-and-mongodb" will run the client and the server at different ports 8080 and 3090 respectively.

## In my next document I will provide steps to launch the react client from the express server and just listen the server port.




